CREATE TABLE wordpress.wp_sklep_categories
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    lp INT NOT NULL,
    published INT DEFAULT 0 NOT NULL,
    params TEXT NOT NULL
);

CREATE TABLE wordpress.wp_sklep_products
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    vendor_id INTEGER(11) NOT NULL,
    category_id INTEGER(11) NOT NULL,
    name VARCHAR(255) NOT NULL,
    price FLOAT NOT NULL,
    published INT DEFAULT 0 NOT NULL,
    params TEXT NOT NULL
);

CREATE TABLE wordpress.wp_sklep_order
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    transaction_id INTEGER(11) NOT NULL,
    vendor_id INTEGER(11) NOT NULL,
    status INTEGER(3) DEFAULT 1 NOT NULL,
    value FLOAT NOT NULL,
    params TEXT NOT NULL
);