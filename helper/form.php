<?php

class Cerbud_Sklep_Form
{

    public static function init()
    {
        $name = get_called_class();
        return new $name();
    }

    static public function create($value = null, $options = array())
    {
        if (is_object($options)) {
            $options = get_object_vars($options);
        }
        
        switch ($options['form']) {
            case 'text' :
                self::text($value, $options);
                break;
            case 'number':
                self::number($value, $options);
                break;
            case 'select':
                self::select($value, $options);
                break;
            case 'textarea':
                self::textarea($value, $options);
                break;
            case 'formfield':
                break;
            case 'image':
                self::image($value, $options);
                break;
            case 'date':
                self::date($value, $options);
                break;
            case 'checkbox':
                break;
            case 'radio':
                break;
            default:
                self::text($value, $options);
                break;
        }
    }

    public static function image($value, $options)
    {
        $name = $options['name'] ? $options['name'] : "";
        $default = $options['default'] ? $options['default'] : null;
        $value = $value ? $value : $default;
        $required = $options["required"] ? "required=\"required\" " : "";

        echo "<input id=\"{$name}\" type=\"text\" size=\"36\" name=\"{$name}\" value=\"{$value}\" {$required}/>
		<button id=\"upload_image_button_{$name}\">". __('Upload Image','sklepcerbud') . "</button>";
        ?>
        <script>
            jQuery(document).ready(function() {
                var old_tb_remove = window.tb_remove;

                jQuery('#upload_image_button_<?php echo $name; ?>').click(function() {
                    formfield = jQuery('#<?php echo $name; ?>').attr('name');
                    tb_show('', 'media-upload.php?type=image&TB_iframe=true');
                    return false;
                });

                window.send_to_editor = function(html) {
                    imgurl = jQuery('img',html).attr('src');
                    jQuery('#<?php echo $name; ?>').val(imgurl);
                    old_tb_remove();
                };

                window.tb_remove = function() {
                    old_tb_remove();
                    return false;
                }
            });
        </script>
        <?php
    }

    public static function textarea($value, $options)
    {
        $name = $options['name'] ? $options['name'] : "";
        $default = $options['default'] ? $options['default'] : null;
        $value = $value ? $value : $default;
        $required = $options["required"] ? "required=\"required\" " : "";
        $settings = array(
            'textarea_rows' => 80,
            'editor_height' => 300,
            'media_buttons' => false,
            'textarea_name' => $name
        );

        wp_editor($value, $name, $settings);
        //echo "<textarea class=\"wp-editor-area\" name=\"{$name}\" {$required} style=\"width:100%; height: 300px;\">{$value}</textarea>";
    }

    public static function select($value, $options)
    {
        $name = $options['name'] ? $options['name'] : "";
        $default = $options['default'] ? $options['default'] : null;
        $value = $value ? $value : $default;
        $required = $options["required"] ? "required=\"required\" " : "";

        if (isset($options['options']) && is_array($options['options'])) {
            $options = self::options($value, $options['options']);
        }

        echo "<select name=\"{$name}\" {$required}>{$options}</select>";
    }

    public static function text($value = null, $options = array())
    {
        $options['type'] = 'text';
        self::input($value, $options);
    }

    public static function number($value = null, $options = array())
    {
        $options['type'] = 'number';
        self::input($value, $options);
    }

    public static function date($value = null, $options = array()) 
    {
        $options['class'] = "ui-date " . ($options['class'] ? $options['class'] : "" );
        self::input($value, $options);
    }
    
    public static function input($value = null, $options = array())
    {
        $type = $options['type'] ? $options['type'] : "text";
        $name = $options['name'] ? $options['name'] : "";
        $info = $options['info'] ? $options['info'] : "";
        $class = $options['class'] ? $options['class'] : "";
        $default = $options['default'] ? $options['default'] : "";
        $max = $options["max"] ? $options["max"] : null;
        $min = $options["min"] ? $options["min"] : null;
        $step = $options["step"] ? $options["step"] : null;
        $maxlength = $options["maxlength"] ? $options["maxlength"] : null;
        $pattern = $options["pattern"] ? $options["pattern"] : null;
        $placeholder = $options["placeholder"] ? $options["placeholder"] : null;
        $required = $options["required"] ? $options["required"] : false;
        $autocomplete = $options['autocomplete'] ? $options['autocomplete'] : "off";
        $value = $default || $value;

        echo "<input "
            . "type=\"{$type}\" "
            . "name=\"{$name}\" "
            . "value=\"{$value}\" "
            . "autocomplete=\"{$autocomplete}\" "
            . "class=\"{$class}\" "
            . ($max ? "max=\"{$max}\" " : "")
            . ($min ? "max=\"{$min}\" " : "")
            . ($step ? "size=\"{$step}\" " : "")
            . ($maxlength ? "maxlength=\"{$maxlength}\" " : "")
            . ($pattern ? "pattern=\"{$pattern}\" " : "")
            . ($placeholder ? "placeholder=\"{$placeholder}\" " : "")
            . ($required ? "required=\"required\" " : "")
            . " /> {$info}";
    }

    /**
     * Dodaje do strony skrypt potrzebne do uploadu plików
     */
    public static function media_scripts()
    {
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_script('jquery');
        wp_enqueue_script('jquery-ui', plugins_url( 'assets/jquery-ui.min.js', dirname(__FILE__) ) );
    }

    public static function media_style()
    {
        wp_enqueue_style('thickbox');
        wp_enqueue_style('jquery-ui', plugins_url( 'assets/jquery-ui.min.css', dirname(__FILE__) ) );
        wp_enqueue_style('jquery-ui.structure', plugins_url( 'assets/jquery-ui.structure.min.css', dirname(__FILE__) ) );
        wp_enqueue_style('jquery-ui.theme', plugins_url( 'assets/jquery-ui.theme.min.css', dirname(__FILE__) ) );
    }

    private static function options($value, $options)
    {
        $return = "";

        foreach ($options as $key => $option) {
            $selected = $key == $value ? "selected=\"selected\" " : "";
            $return .= "<option {$selected}value=\"{$key}\">{$option}</option>";
        }

        return $return;
    }
}