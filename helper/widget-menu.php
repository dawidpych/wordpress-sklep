<?php
/**
 * @packed Cerbud_Sklep
 */

class WP_Widget_Menu_Cerbud_Sklep extends WP_Widget {

    public function __construct() {
        parent::__construct('WP_Widget_Menu_Cerbud_Sklepd', "Sklep");
    }

    /**
     * Front-End body
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) {

    }

    /**
     * Updating widget replace old instance with new instace
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();

        return $new_instance;
    }
}