<?php

class Cerbud_Sklep_Products extends Cerbud_Sklep_Model
{
    protected $_table = "sklep_products";
}

class Cerbud_Sklep_Admin_Products extends Cerbud_Sklep_Admin_Factory
{

    protected $_CONFIG_FILE_NAME = 'config.json';

    var $creds;

    public function __construct()
    {
        parent::__construct();
    }

    public function view()
    {
        global $products, $wpdb;
        include_once('admin/products.php');
    }

    public function edit()
    {
        global $products, $categories;

        $json = file_get_contents(trailingslashit(WP_PLUGIN_DIR . '/sklep') . $this->_CONFIG_FILE_NAME);
        $options = json_decode($json);
        $options = $options->products;

        if (isset($_GET['id'])) {
            $data = $products->get_item(array('id' => htmlentities($_GET['id'])));
            $data->params = json_decode($data->params);
        } else {
            $data = new stdClass();
            $data->params = json_decode('{}');
        }

        include_once('admin/add_product.php');
    }

    public function save()
    {
        global $products;

        $data = $_POST;

        $data['published'] = (int)$data['published'];
        $data['category_id'] = (int)$data['category_id'];
        $data['client_id'] = (int)$data['client_id'];
        $data['price'] = (float)$data['price'];
        $data['id'] = (int)$data['id'];
        $data['params'] = json_encode($this->get_params());

        $data = $this->clean_data($data);

        unset($data['action']);

        if (isset($data['save']) && !(bool)$data['id']) {
            unset($data['save']);
            unset($data['id']);

            $ret = $products->add_item($data);

            if ($ret) {
                wp_redirect('?page=shop_products_edit&id=' . $ret);
            } else {
                wp_redirect('?page=shop_products_edit');
            }
            die();
        } elseif (isset($data['save']) && (bool)$data['id']) {

            unset($data['save']);

            $products->update_item($data);

            wp_redirect('?page=shop_products_edit&id=' . $data['id']);
        } elseif (isset($data['remove']) && (bool)$data['id']) {

            unset($data['remove']);

            $products->remove_item($data);

            wp_redirect('?page=shop_products');
        }
        die();
    }

    public function get_params()
    {
        $params = array();
        $data = $_POST;

        $json = file_get_contents(trailingslashit(WP_PLUGIN_DIR . '/sklep') . $this->_CONFIG_FILE_NAME);
        $options = json_decode($json);

        foreach ($options->products->additional as $adds) {
            if ($adds->form != 'fieldset') {
                $params[$adds->name] = $data[$adds->name];
            } else {
                foreach ($adds->items as $item) {
                    $params[$item->name] = $data[$item->name];
                }
            }
        }

        foreach ($options->products->images as $adds) {
            $params[$adds->name] = $data[$adds->name];
        }

        foreach ($options->products->describes as $adds) {
            $params[$adds->name] = $data[$adds->name];
        }

        return $params;
    }

    public function clean_data($data)
    {
        $json = file_get_contents(trailingslashit(WP_PLUGIN_DIR . '/sklep') . $this->_CONFIG_FILE_NAME);
        $options = json_decode($json);

        foreach ($options->products->additional as $adds) {
            if ($adds->form != 'fieldset') {
                unset($data[$adds->name]);
            } else {
                foreach ($adds->items as $item) {
                    unset($data[$item->name]);
                }
            }
        }

        foreach ($options->products->images as $adds) {
            unset($data[$adds->name]);
        }

        foreach ($options->products->describes as $adds) {
            unset($data[$adds->name]);
        }

        return $data;
    }

    public function add_dashboard_menu()
    {
        add_submenu_page(
            'shop',
            __('Products', 'sklepcerbud'),
            __('Products', 'sklepcerbud'),
            'vendor',
            'shop_products',
            array($this, 'view')
        );
        add_submenu_page(
            'shop',
            __('Add Product', 'sklepcerbud'),
            __('Add Product', 'sklepcerbud'),
            'vendor',
            'shop_products_edit',
            array($this, 'edit')
        );
        add_action('admin_action_shop_products_edit', array($this, 'save'));
    }
}

$products = Cerbud_Sklep_Products::init();

if (is_admin()) {
    $products_admin = Cerbud_Sklep_Admin_Products::init();
}
