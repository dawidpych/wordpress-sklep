<?php

abstract class Cerbud_Sklep_Admin_Factory extends Cerbud_Sklep_Factory {

    public function __construct() {
        add_action('admin_menu', array($this, 'add_dashboard_menu'));
    }

    abstract public function view();

    abstract public function add_dashboard_menu();
}