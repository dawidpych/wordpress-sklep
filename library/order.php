<?php
class Cerbud_Sklep_Order extends Cerbud_Sklep_Model {
    protected $_table = "sklep_orders";
}

class Cerbud_Sklep_Admin_Order extends Cerbud_Sklep_Admin_Factory {

    public function view() {
        global $orders, $wpdb;
        include_once('admin/dashboard.php');
    }

    public function edit() {
        global $orders;

        if (isset($_GET['id'])) {
            $data = $orders->get_item(array('id'=>htmlentities($_GET['id'])));
        } else {
            $data = new stdClass();
        }

        include_once('admin/edit_order.php');
    }

    public function add_dashboard_menu(){
        add_menu_page(
            __('Sklep Dashboard', 'sklepcerbud'),
            __('Shop','sklepcerbud'),
            'vendor',
            'shop',
            array($this, 'view'),
            'dashicons-cart',
            21
        );
    }



}

$orders = Cerbud_Sklep_Order::init();

if( is_admin() ) {
    $order_admin = Cerbud_Sklep_Admin_Order::init();
}