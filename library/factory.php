<?php

interface Model
{

    public function get_items($option);

    public function get_item($option);

    public function add_item($option);

    public function remove_item($option);

    public function update_item($option);
}

abstract class Cerbud_Sklep_Factory
{

    public static function init()
    {
        $name = get_called_class();
        return new $name();
    }
}

abstract class Cerbud_Sklep_Model extends Cerbud_Sklep_Factory implements Model
{

    protected $_table;

    /**
     * example:
     * $options['where'] = array('id','=',1);
     * $options['where'] = array(array('id','=',1), 'AND' );
     **/
    public function get_items($option = array())
    {
        global $wpdb;

        $select = isset($option['select']) ? ' ' . $option['select'] . ' ' : ' * ';
        $page = isset($option['page']) ? $option['page'] : 1;
        $limit = isset($option['limit']) ? $option['limit'] : 20;
        $order_by = isset($option['order_by']) ? $option['order_by'] : $wpdb->prefix . $this->_table . '.id';
        $dir = isset($option['dir']) ? $option['dir'] : 'DESC';
        $where = isset($option['where']) ? ' WHERE ' . $this->_where_join($option['where']) . ' ' : '';
        $join = '';

        if (isset($option['join']) && is_array($option['join'][0])) {
            $join = ' ';
            foreach ($option['join'] as $item) {
                $join .= $item[0] . ' JOIN ' . $item[1] . ' ON ' . join(' ', $item[2]) . ' ';
            }
        } elseif (isset($option['join']) && !is_array($option['join'][0])) {
            $join = ' ' . $option['join'][0] . ' JOIN ' . $option['join'][1] . ' ON ' . join(' ', $option['join'][2]) . ' ';
        }

        $group_by = isset($option['group_by']) ? ' GROUP BY ' . join($option['group_by'], ',') . ' ' : '';

        $sql = "SELECT" . $select
            . "FROM {$wpdb->prefix}{$this->_table} "
            . $join
            . $where
            . $group_by
            . "ORDER BY {$order_by} {$dir} "
            . "LIMIT {$limit} "
            . "OFFSET " . (($page - 1) * $limit);

        return $wpdb->get_results($sql, OBJECT);
    }

    public function get_item($option)
    {
        global $wpdb;

        $id = $option['id'];

        $sql = "SELECT * "
            . "FROM {$wpdb->prefix}{$this->_table} "
            . "WHERE {$wpdb->prefix}{$this->_table}.id = {$id}";

        return $wpdb->get_row($sql, OBJECT);
    }

    public function add_item($option)
    {
        global $wpdb;

        $data = $wpdb->insert($wpdb->prefix . $this->_table, $option);

        if ($data) {
            return $wpdb->insert_id;
        } else {
            return $data;
        }
    }

    public function remove_item($option)
    {
        global $wpdb;

        $wpdb->delete($wpdb->prefix . $this->_table, $option);
    }

    public function update_item($option)
    {
        global $wpdb;

        $where = array('id' => $option['id']);

        unset($option['id']);

        $wpdb->update($wpdb->prefix . $this->_table, $option, $where);
    }

    public function insert_id()
    {
        global $wpdb;
        return $wpdb->insert_id;
    }

    private function _where_join($data)
    {
        $return = "";

        if (is_array($data)) {
            foreach ($data as $item) {
                $return .= $this->_where_join($item);
            }
        } else {
            $return .= $data;
        }

        return $return;
    }
}