<?php
/**
 * @package Sklep Polska Cegła
 */

class Cerbud_Sklep_Category extends Cerbud_Sklep_Model {
    protected $_table = "sklep_categories";

    public function get_products($params = array()) {
        global $wpdb, $products;

        $option = array();

        if (isset($params['category_id'])) {
            $option['where'] = array(array('category_id','=',$params['category_id']));
        }

        if (isset($params['vendor_id'])) {
            if(count($option['where'])) {
                $option['where'][] = 'AND';
            }
            $option['where'][] = array('category_id','=',$params['category_id']);
        }

        $option['order_by']    = $wpdb->prefix . 'sklep_products' . '.price';
        $option['dir']         = isset($params['dir']) ? $params['dir'] : 'DESC';

        return $products->get_items($option);
    }
}

class Cerbud_Sklep_Admin_Category extends Cerbud_Sklep_Admin_Factory {

    public function view() {
        global $categories, $wpdb;
        include_once('admin/categories.php');
    }

    public function edit() {
        global $categories;

        if (isset($_GET['id'])) {
            $data = $categories->get_item(array('id'=>htmlentities($_GET['id'])));
        } else {
            $data = new stdClass();
        }

        include_once('admin/add_category.php');
    }

    public function save() {
        global $categories;

        $data = $_POST;

        $data['published']          = (int) $data['published'];
        $data['sklep_category_id']  = (int) $data['sklep_category_id'];
        $data['client_id']          = (int) $data['client_id'];
        $data['id']                 = (int) $data['id'];
        $data['params']             = '{}';
        $data['lp']                 = 2;

        unset($data['action']);

        if ( isset($data['save']) && !(bool)$data['id'] ) {
            unset($data['save']);
            unset($data['id']);

            $ret = $categories->add_item($data);

            if($ret) {
                wp_redirect('?page=shop_categories_edit&id=' . $ret );
            } else {
                wp_redirect('?page=shop_categories_edit');
            }
        } elseif(isset($data['save']) && (bool)$data['id']) {

            unset($data['save']);

            $data['last_date'] = date('Y-m-d G:i:s');

            $categories->update_item($data);

            wp_redirect('?page=shop_categories_edit&id=' . $data['id']);
        } elseif ( isset($data['remove']) && (bool)$data['id']) {

            unset($data['remove']);

            $categories->remove_item($data);

            wp_redirect('?page=shop_categories');

        }
        die();
    }

    public function add_dashboard_menu(){

        add_submenu_page(
            'shop',
            __('Categories', 'sklepcerbud'),
            __('Categories', 'sklepcerbud'),
            'administrator',
            'shop_categories',
            array($this,'view')
        );
        add_submenu_page(
            'shop',
            __('Add Categories', 'sklepcerbud'),
            __('Add Categories', 'sklepcerbud'),
            'administrator',
            'shop_categories_edit',
            array($this,'edit')
        );
        add_action('admin_action_shop_categories_edit', array($this, 'save'));
    }
}

$categories = Cerbud_Sklep_Category::init();

if( is_admin() ) {
    $categories_admin = Cerbud_Sklep_Admin_Category::init();
}