<?php
$sql = '
    SELECT COUNT(*) as allitems,
      (SELECT COUNT(*) FROM ' . $wpdb->prefix . 'sklep_orders WHERE published = 1) as active,
      (SELECT COUNT(*) FROM ' . $wpdb->prefix . 'sklep_orders WHERE published = 0) as deactive
    FROM ' . $wpdb->prefix . 'sklep_orders group by order_id
';

$res = $wpdb->get_results( $sql );

var_dump($sql);

$option = array(
    'select' => $wpdb->prefix . 'sklep_orders.*, c.name as parent_name, u.*',
    'join' => array(
        array('left', $wpdb->prefix . 'sklep_categories' . ' AS c ', array('c.sklep_category_id', '=', $wpdb->prefix . 'sklep_categories.id') ),
        array('left', $wpdb->users . ' AS u ', array( $wpdb->prefix . 'sklep_categories.client_id', '=', 'u.ID') ),
    )
);

if(isset($_GET['orderby']) && (bool) $_GET['orderby']) {
    $order_by = $_GET['orderby'];
    $option['order_by'] = $order_by;
}

if(isset($_GET['order']) && (bool) $_GET['order']) {
    $order = $_GET['order'];
    $option['dir'] = $order;

    if($order == 'desc') {
        $new_order = 'asc';
    } else {
        $new_order = 'desc';
    }
}

if(isset($_GET['published'])) {
    $published = (int) $_GET['published'];
    $option['where'] = array($wpdb->prefix . 'sklep_categories.' . 'published','=',$published);
}

$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;

if(isset($_GET['pagenum'])) {
    $option['page'] = $pagenum;
}

$num_of_pages = ceil( $res[0]->allitems / 20 );
?>
<div class="wrap">
    <h1><?php echo __('Cerbud - Sklep'); ?></h1>
    <ul class="subsubsub">
        <li class="all">
            <a href="<?php echo self_admin_url( 'admin.php?page=shop'); ?>" <?php if(!isset($_GET['published'])) : ?>class="current"<?php endif; ?>><?php echo __('Wszystkie', 'cerbud'); ?> <span class="count">(<?php echo $res[0]->allitems; ?>)</span></a> |
        </li>
        <li class="publish">
            <a href="<?php echo self_admin_url( 'admin.php?page=shop'); ?>&published=1" <?php if($published === 1) : ?>class="current"<?php endif; ?>><?php echo __('Aktywne', 'cerbud'); ?> <span class="count">(<?php echo $res[0]->active; ?>)</span></a> |
        </li>
        <li class="unpublish">
            <a href="<?php echo self_admin_url( 'admin.php?page=shop'); ?>&published=0" <?php if($published === 0 && isset($_GET['published'])) : ?>class="current"<?php endif; ?>><?php echo __('Nie aktywne', 'cerbud'); ?> <span class="count">(<?php echo $res[0]->deactive; ?>)</span></a>
        </li>
    </ul>
    <table class="wp-list-table widefat fixed striped posts">
        <thead>
            <tr>
                <td id="cb" class="manage-column column-cb check-column">
                    <label class="screen-reader-text" for="cb-select-all-1">Wybierz wszystko</label>
                    <input id="cb-select-all-1" type="checkbox">
                </td>
                <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <a href="http://wp.local/wp-admin/edit.php?orderby=title&amp;order=asc">
                        <span><?php echo __('Nr. zamówienia'); ?></span>
                        <span class="sorting-indicator"></span>
                    </a>
                </th>
                <th scope="col" id="kwota" class="manage-column column-tags">
                    <a href="http://wp.local/wp-admin/edit.php?orderby=date&amp;order=desc">
                        <span><?php echo __('Kwota'); ?></span>
                        <span class="sorting-indicator"></span>
                    </a>
                </th>
                <th scope="col" id="author" class="manage-column column-author">
                    <?php echo __('Klient'); ?>
                </th>
                <th scope="col" id="categories" class="manage-column column-categories">
                    <?php echo __('Status'); ?>
                </th>
                <th scope="col" id="tags" class="manage-column column-tags">
                    <a href="http://wp.local/wp-admin/edit.php?orderby=date&amp;order=desc">
                        <span><?php echo __('Data'); ?></span>
                        <span class="sorting-indicator"></span>
                    </a>
                </th>
                <!--
                <th scope="col" id="comments" class="manage-column column-comments num sortable desc">
                    <a href="http://wp.local/wp-admin/edit.php?orderby=comment_count&amp;order=asc">
                        <span>
                            <span class="vers comment-grey-bubble" title="Komentarze">
                                <span class="screen-reader-text">Komentarze</span>
                            </span>
                        </span>
                        <span class="sorting-indicator"></span>
                    </a>
                </th>
                -->
            </tr>
        </thead>
    </table>
</div>