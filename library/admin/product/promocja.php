<table id="sklep-produkt-promocje" class="form-table hide">
    <tbody>
        <tr>
            <th>
                <h1><?php echo __($data->id ? 'Edycja Produktu' : 'Nowy Produkt', 'cerbud'); ?></h1>
            </th>
            <td style="text-align: right">
                <button type="button" class="button button-primary add-promotion">
                    <?php echo __('Dodaj Promocje','cerbud'); ?>
                </button>
                <?php if ((bool)$data->id) : ?>
                <button type="submit" name="remove" class="button button-secondary">
                    <?php echo __('Usuń Produkt', 'cerbud'); ?>
                </button>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <?php foreach($options->promotions ? $options->promotions : [] as $adds) : ?>
            <td>
                <?php
                $value = $data->params->{$adds->name} ? $data->params->{$adds->name} : null;
                ?>
                <label><?php echo $adds->label; ?></label> <div><?php Cerbud_Sklep_Form::create($value,$adds); ?></div>
            </td>
            <?php endforeach; ?>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="2" class="text-right">
                <button type="button" class="button button-primary add-promotion">
                    <?php echo __('Dodaj Promocje','cerbud'); ?>
                </button>
            </td>
        </tr>
    </tfoot>
</table>
<script>
    (function ($) {
        var date = "";
        var html = "";
            <?php foreach($options->promotions as $adds) : ?>
                html += '<td><label><?php echo __($adds->label, "cerbud"); ?></label> <div><?php Cerbud_Sklep_Form::create($value,$adds); ?></div></td>';
            <?php endforeach; ?>
        
        function startdate() {
            $(".ui-date").datepicker({
                "changeMonth" : true,
                "changeYear" : true,
                "dateFormat": "dd-mm-yy"
            });
        }
        
        $(".add-promotion").on("click", function () {
            $("#sklep-produkt-promocje tbody").append("<tr>" + html + "</tr>");
        });
        
        $("#sklep-produkt-promocje tbody").bind('DOMSubtreeModified',function(){
            startdate();
        });
        
        startdate();
        
    })(jQuery);
</script>