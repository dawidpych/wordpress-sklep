<?php

class Cerbud_Sklep_Nav_Menu {

    public function __construct(){
        $this->add_nav_menu_meta_boxes();
    }

    public function add_nav_menu_meta_boxes() {
        add_meta_box(
            'cerbud-sklep-nav-links',
            __('Sklep','sklepcerbud'),
            array('Cerbud_Sklep_Nav_Menu', 'nav_menu_links'),
            'nav-menus',
            'side',
            'low'
        );
    }

    public function nav_menu_links() {
        $removed_args = array(
            'action',
            'customlink-tab',
            'edit-menu-item',
            'menu-item',
            'page-tab',
            '_wpnonce',
        );

        ?>
        <div id="posttype-cerbud-sklep-nav-links" class="posttypediv">
            <div id="tabs-panel-wishlist-login" class="tabs-panel tabs-panel-active">
                <ul id ="wishlist-login-checklist" class="categorychecklist form-no-clear">
                    <li>
                        <label class="menu-item-title">
                            <input type="checkbox" class="menu-item-checkbox" name="menu-item[-1][menu-item-object-id]" value="-1"> <?php echo __('Link do koszyka', 'cerbudsklep'); ?>
                        </label>
                        <input type="hidden" class="menu-item-type" name="menu-item[-1][menu-item-type]" value="custom">
                        <input type="hidden" class="menu-item-title" name="menu-item[-1][menu-item-title]" value="<?php echo  __('Koszyk', 'cerbudsklep'); ?>">
                        <input type="hidden" class="menu-item-url" name="menu-item[-1][menu-item-url]" value="<?php bloginfo('wpurl'); ?>/?basket=show">
                        <input type="hidden" class="menu-item-classes" name="menu-item[-1][menu-item-classes]" value="cerbud-sklep-nav-link">
                    </li>
                    <li>
                        <label class="menu-item-title">
                            <input type="checkbox" class="menu-item-checkbox" name="menu-item[-2][menu-item-object-id]" value="-2"> <?php echo __('Kategoria'); ?>
                        </label>
                        <input type="hidden" class="menu-item-type" name="menu-item[-2][menu-item-type]" value="custom">
                        <input type="hidden" class="menu-item-title" name="menu-item[-2][menu-item-title]" value="<?php echo __('Kategoria', 'cerbudsklep'); ?>">
                        <input type="hidden" class="menu-item-url" name="menu-item[-2][menu-item-url]" value="<?php bloginfo('wpurl'); ?>/?category=2">
                        <input type="hidden" class="menu-item-classes" name="menu-item[-2][menu-item-classes]" value="cerbud-sklep-nav-link">
                    </li>
                </ul>
            </div>
            <p class="button-controls">
                <span class="list-controls">
                    <a href="<?php
                    echo esc_url( add_query_arg(
                        array(
                            'cerbud-sklep-nav-links-tab' => 'all',
                            'selectall' => 1,
                        ),
                        remove_query_arg( $removed_args )
                    ));
                    ?>#posttype-cerbud-sklep-nav-links" class="select-all"><?php _e('Select All'); ?></a>
                </span>

                <span class="add-to-menu">
        			<input type="submit" class="button-secondary submit-add-to-menu right" value="<?php esc_attr_e( 'Add to Menu' ); ?>" name="add-post-type-menu-item" id="submit-posttype-cerbud-sklep-nav-links">
        			<span class="spinner"></span>
                </span>
            </p>
        </div>
    <?php }

}