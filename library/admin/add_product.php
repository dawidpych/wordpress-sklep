<?php if( (get_current_user_id() != (int) $data->client_id) && $data->id && !is_super_admin()) : ?>
<div id="sklep-add-products" class="wrap">
    <h3><?php echo __('Nie tego produktu szukasz.'); ?></h3>
</div>
<?php else: ?>
<div id="sklep-add-products" class="wrap">
    <style type="text/css">
        .hide {
            display: none;
        }
    </style>
    <h2 class="nav-tab-wrapper">
        <a href="#sklep-produkt-parametry-podstawowe"
           class="nav-tab nav-tab-active"><?php echo __('Parametry podstawowe', 'sklepcerbud'); ?></a>
        <a href="#sklep-produkt-parametry-dodatkowe"
           class="nav-tab"><?php echo __('Parametry dodatkowe', 'sklepcerbud'); ?></a>
        <a href="#sklep-produkt-opisy" class="nav-tab"><?php echo __('Opisy', 'sklepcerbud'); ?></a>
        <a href="#sklep-produkt-zdjecia" class="nav-tab"><?php echo __('Zdjęcia', 'sklepcerbud'); ?></a>
        <a href="#sklep-produkt-promocje" class="nav-tab"><?php echo __('Promocje', 'sklepcerbud'); ?></a>
    </h2>

    <div class="nav-content">
        <form method="post" action="<?php echo self_admin_url('admin.php'); ?>">
            <?php include('product/promocja.php'); ?>
            <table id="sklep-produkt-zdjecia" class="form-table hide">
                <tbody>
                <tr>
                    <th>
                        <h1><?php echo __($data->id ? 'Edycja Produktu' : 'Nowy Produkt', 'cerbud'); ?></h1>
                    </th>
                    <td style="text-align: right">
                        <?php if ((bool)$data->id) : ?>
                            <button type="submit" name="remove"
                                    class="button button-secondary"><?php echo __('Usuń Produkt', 'cerbud'); ?></button>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php foreach($options->images as $adds) : ?>
                    <tr>
                        <th>
                            <?php echo $adds->label; ?>
                        </th>
                        <td>
                            <?php
                            $value = $data->params->{$adds->name} ? $data->params->{$adds->name} : null;
                            ?>
                            <?php Cerbud_Sklep_Form::create($value,$adds); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <table id="sklep-produkt-opisy" class="form-table hide">
                <tbody>
                <tr>
                    <th>
                        <h1><?php echo __($data->id ? 'Edycja Produktu' : 'Nowy Produkt', 'cerbud'); ?></h1>
                    </th>
                    <td style="text-align: right">
                        <?php if ((bool)$data->id) : ?>
                            <button type="submit" name="remove"
                                    class="button button-secondary"><?php echo __('Usuń Produkt', 'cerbud'); ?></button>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php foreach($options->describes as $adds) : ?>
                    <?php if ($adds->form == 'fieldset') : ?>
                        <tr>
                            <th>
                                <?php echo $adds->label; ?>
                            </th>
                            <td>
                                <?php foreach( $adds->items as $item) : ?>
                                    <div>
                                        <?php echo $item->label; ?>
                                    </div>
                                    <div>
                                        <?php
                                        $value = $data->params->{$item->name} ? $data->params->{$item->name} : null;
                                        ?>
                                        <?php Cerbud_Sklep_Form::create($value,$item); ?>
                                    </div>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                    <?php else: ?>
                        <tr>
                            <th>
                                <?php echo $adds->label; ?>
                            </th>
                            <td>
                                <?php
                                $value = $data->params->{$adds->name} ? $data->params->{$adds->name} : null;
                                ?>
                                <?php Cerbud_Sklep_Form::create($value,$adds); ?>
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
                </tbody>
            </table>
            <table id="sklep-produkt-parametry-dodatkowe" class="form-table hide">
                <tbody>
                <tr>
                    <th>
                        <h1><?php echo __($data->id ? 'Edycja Produktu' : 'Nowy Produkt', 'cerbud'); ?></h1>
                    </th>
                    <td style="text-align: right">
                        <?php if ((bool)$data->id) : ?>
                            <button type="submit" name="remove"
                                    class="button button-secondary"><?php echo __('Usuń Produkt', 'cerbud'); ?></button>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php foreach($options->additional as $adds) : ?>
                    <?php if ($adds->form == 'fieldset') : ?>
                    <tr>
                        <th>
                            <?php echo $adds->label; ?>
                        </th>
                        <td>
                            <?php foreach( $adds->items as $item) : ?>
                                <div>
                                    <?php echo $item->label; ?>
                                </div>
                                <div>
                                    <?php
                                    $value = $data->params->{$item->name} ? $data->params->{$item->name} : null;
                                    ?>
                                    <?php Cerbud_Sklep_Form::create($value,$item); ?>
                                </div>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                    <?php else: ?>
                    <tr>
                        <th>
                            <?php echo $adds->label; ?>
                        </th>
                        <td>
                            <?php
                            $value = $data->params->{$adds->name} ? $data->params->{$adds->name} : null;
                            ?>
                            <?php Cerbud_Sklep_Form::create($value,$adds); ?>
                        </td>
                    </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
                </tbody>
            </table>
            <table id="sklep-produkt-parametry-podstawowe" class="form-table">
                <tbody>
                <tr>
                    <th>
                        <h1><?php echo __($data->id ? 'Edycja Produktu' : 'Nowy Produkt', 'cerbud'); ?></h1>
                    </th>
                    <td style="text-align: right">
                        <?php if ((bool)$data->id) : ?>
                            <button type="submit" name="remove"
                                    class="button button-secondary"><?php echo __('Usuń Produkt', 'cerbud'); ?></button>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Wprowadź tytuł', 'cerbud'); ?></th>
                    <td><input type="text" name="name" size="30" value="<?php echo $data->name ?>" spellcheck="true"
                               autocomplete="off" required="required"></td>
                </tr>
                <tr>
                    <th><?php echo __('Cena brutto', 'cerbud'); ?></th>
                    <td><input type="number" name="price" size="30" value="<?php echo $data->price ?>"
                               required="required" step="any" min="0.01"></td>
                </tr>
                <tr>
                    <th><?php echo __('Aktywne', 'cerbud'); ?></th>
                    <td>
                        <fieldset>
                            <label for="activeTrue">
                                <input id="activeTrue" type="radio" name="published" size="30"
                                       value="1" <?php echo $data->published ? 'checked="checked"' : ''; ?>> <?php echo __('Tak', 'cerbud'); ?>
                            </label>
                            <label for="activeFalse">
                                <input id="activeFalse" type="radio" name="published" size="30"
                                       value="0" <?php echo !$data->published ? 'checked="checked"' : ''; ?>> <?php echo __('Nie', 'cerbud'); ?>
                            </label>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Kategoria', 'cerbud'); ?></th>
                    <td>
                        <select name="category_id">
                            <!-- <option value='0' <?php echo !(bool)$data->category_id ? 'selected="selected"' : ''; ?>><?php echo __('Brak', 'cerbud'); ?></option>-->
                            <?php foreach ($categories->get_items($option) as $item): ?>
                                <?php if ((int)$data->id != (int)$item->id) : ?>
                                    <option
                                        <?php echo (int)$data->category_id == (int)$item->id ? 'selected="selected"' : ''; ?>
                                        value='<?php echo $item->id; ?>'
                                    ><?php echo $item->name; ?></option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                </tbody>
            </table>
            <p class="submit">
                <button type="submit" name="save"
                        class="button button-primary button-large"><?php echo __('Zapisz', 'cerbud'); ?></button>
            </p>
            <input type="hidden" name="client_id" value="<?php echo $data->client_id ? $data->client_id : get_current_user_id(); ?>">
            <input type="hidden" name="id" value="<?php echo $data->id; ?>">
            <input type="hidden" name="action" value="shop_products_edit"/>
        </form>
    </div>
    <script>
        (function ($) {
            var divId = '#sklep-add-products',
                nav = $(divId + ' .nav-tab-wrapper a'),
                contents = $(divId + ' .nav-content .form-table'),
                classActive = 'nav-tab-active',
                classHide = 'hide';

            var click = function () {
                var me = $(this),
                    id = me.attr('href');

                nav.removeClass(classActive);
                me.addClass(classActive)

                contents
                    .addClass(classHide);
                $(id).removeClass(classHide);

                return false;
            };

            $(nav).on('click', click);
        })(jQuery);
    </script>
</div>
<?php endif; ?>