<?php
$option = array(
    'where'=>array('client_id','=',get_current_user_id())
);
?>
<div class="wrap">
    <div>
        <form method="post" action="<?php echo self_admin_url( 'admin.php' ); ?>">
            <table class="form-table">
                <tbody>
                    <tr>
                        <th><h1><?php echo __($data->id ? 'Edycja Kategorii' : 'Nowa Kategoria', 'cerbud'); ?></h1></th>
                        <td style="text-align: right">
                            <?php if ((bool)$data->id) : ?>
                            <button type="submit" name="remove" class="button button-secondary"><?php echo __('Usuń kategorie', 'cerbud'); ?></button>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Wprowadź tytuł', 'cerbud'); ?></th>
                        <td><input type="text" name="name" size="30" value="<?php echo $data->name ?>" spellcheck="true" autocomplete="off" required="required"></td>
                    </tr>
                    <tr>
                        <th><?php echo __('Aktywne', 'cerbud'); ?></th>
                        <td>
                            <fieldset>
                                <label for="activeTrue">
                                    <input id="activeTrue" type="radio" name="published" size="30" value="1" <?php echo $data->published ? 'checked="checked"' : ''; ?>> <?php echo __('Tak', 'cerbud'); ?>
                                </label>
                                <label for="activeFalse">
                                    <input id="activeFalse" type="radio" name="published" size="30" value="0" <?php echo !$data->published ? 'checked="checked"' : ''; ?>> <?php echo __('Nie', 'cerbud'); ?>
                                </label>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Kategoria nadrzędna', 'cerbud'); ?></th>
                        <td>
                            <select name="sklep_category_id">
                                <option value='0' <?php echo !(bool) $data->sklep_category_id ? 'selected="selected"' : ''; ?>><?php echo __('Brak', 'cerbud'); ?></option>
                                <?php foreach ($categories->get_items($option) as $item): ?>
                                    <?php if ((int)$data->id != (int)$item->id) : ?>
                                        <option
                                            <?php echo (int) $data->sklep_category_id == (int)$item->id ? 'selected="selected"' : ''; ?>
                                            value='<?php echo $item->id; ?>'
                                        ><?php echo $item->name; ?> (<?php echo $item->id ?>)</option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <p class="submit">
                <button type="submit" name="save" class="button button-primary button-large"><?php echo __('Zapisz', 'cerbud'); ?></button>
            </p>
            <input type="hidden" name="client_id" value="<?php echo get_current_user_id(); ?>">
            <input type="hidden" name="id" value="<?php echo $data->id; ?>">
            <input type="hidden" name="action" value="shop_categories_edit" />
        </form>
    </div>
</div>