<?php
$sql = '
    SELECT COUNT(*) as allitems,
      (SELECT COUNT(*) FROM ' . $wpdb->prefix . 'sklep_products WHERE published = 1' . (!is_super_admin() ? ' AND client_id = ' . get_current_user_id() : "") . ') as active,
      (SELECT COUNT(*) FROM ' . $wpdb->prefix . 'sklep_products WHERE published = 0' . (!is_super_admin() ? ' AND client_id = ' . get_current_user_id() : "") . ') as deactive
    FROM ' . $wpdb->prefix . 'sklep_products'. (!is_super_admin() ? ' WHERE client_id = ' . get_current_user_id() : "") . '
';

$res = $wpdb->get_results($sql);

$option = array(
    'select' => $wpdb->prefix . 'sklep_products.*, c.name as parent_name, u.*',
    'join' => array(
        array('left', $wpdb->prefix . 'sklep_categories' . ' AS c ', array('c.id', '=', $wpdb->prefix . 'sklep_products.category_id')),
        array('left', $wpdb->users . ' AS u ', array($wpdb->prefix . 'sklep_products.client_id', '=', 'u.ID')),
    )
);

if (!is_super_admin()) {
    $option['where'] = array(
        array($wpdb->prefix . 'sklep_products.client_id', '=', get_current_user_id())
    );
}

if (isset($_GET['orderby']) && (bool)$_GET['orderby']) {
    $order_by = $_GET['orderby'];
    $option['order_by'] = $order_by;
}

if (isset($_GET['order']) && (bool)$_GET['order']) {
    $order = $_GET['order'];
    $option['dir'] = $order;

    if ($order == 'desc') {
        $new_order = 'asc';
    } else {
        $new_order = 'desc';
    }
}

if (isset($_GET['published'])) {
    $published = (int)$_GET['published'];
    $option['where'] = array();

    if (!is_super_admin()) {
        $option['where'][] = array($wpdb->prefix . 'sklep_products.client_id', '=', get_current_user_id());
        $option['where'][] = ' AND ';
    }

    $option['where'][] = array($wpdb->prefix . 'sklep_products.published', '=', $published);
}

$pagenum = isset($_GET['pagenum']) ? absint($_GET['pagenum']) : 1;

if (isset($_GET['pagenum'])) {
    $option['page'] = $pagenum;
}

$num_of_pages = ceil($res[0]->allitems / 20);
?>
<div class="wrap">
    <h1>
        <?php echo __('Produkty', 'cerbud'); ?>
        <a href="<?php echo self_admin_url('admin.php?page=shop_products_edit'); ?>"
           class="page-title-action"><?php echo __('Dodaj nowy produkt', 'cerbud'); ?></a>
    </h1>
    <ul class="subsubsub">
        <li class="all">
            <a href="<?php echo self_admin_url('admin.php?page=shop_products'); ?>"
               <?php if (!isset($_GET['published'])) : ?>class="current"<?php endif; ?>><?php echo __('Wszystkie', 'cerbud'); ?>
                <span class="count">(<?php echo $res[0]->allitems; ?>)</span></a> |
        </li>
        <li class="publish">
            <a href="<?php echo self_admin_url('admin.php?page=shop_products'); ?>&published=1"
               <?php if ($published === 1) : ?>class="current"<?php endif; ?>><?php echo __('Aktywne', 'cerbud'); ?>
                <span class="count">(<?php echo $res[0]->active; ?>)</span></a> |
        </li>
        <li class="unpublish">
            <a href="<?php echo self_admin_url('admin.php?page=shop_products'); ?>&published=0"
               <?php if ($published === 0 && isset($_GET['published'])) : ?>class="current"<?php endif; ?>><?php echo __('Nie aktywne', 'cerbud'); ?>
                <span class="count">(<?php echo $res[0]->deactive; ?>)</span></a>
        </li>
    </ul>
    <table class="wp-list-table widefat fixed striped posts">
        <thead>
        <tr>
            <td id="cb" class="manage-column column-cb check-column">

            </td>
            <th scope="col" id="title"
                class="manage-column column-title column-primary sortable <?php echo $order_by == 'name' ? $order : ''; ?>">
                <a href="<?php echo self_admin_url('admin.php?page=shop_products'); ?>&orderby=name&order=<?php echo $order_by == 'name' ? $new_order : 'desc'; ?><?php if (isset($_GET['published'])) : ?>&published=<?php echo $published; ?><?php endif; ?>">
                    <span><?php echo __('Nazwa', 'cerbud'); ?></span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="kwota" class="manage-column column-tags">
                <?php echo __('Kategoria', 'cerbud'); ?>
            </th>
            <th scope="col" id="author" class="manage-column column-author">
                <?php echo __('Klient', 'cerbud'); ?>
            </th>
            <th scope="col" id="categories" class="manage-column column-categories">
                <?php echo __('Status', 'cerbud'); ?>
            </th>
            <th scope="col" id="cena" class="manage-column column-categories">
                <?php echo __('Cena', 'cerbud'); ?>
            </th>
            <th scope="col" id="tags"
                class="manage-column column-tags sortable <?php echo $order_by == 'last_date' ? $order : ''; ?>">
                <a href="<?php echo self_admin_url('admin.php?page=shop_products'); ?>&orderby=last_date&order=<?php echo $order_by == 'last_date' ? $new_order : 'desc'; ?><?php if (isset($_GET['published'])) : ?>&published=<?php echo $published; ?><?php endif; ?>">
                    <span><?php echo __('Data', 'cerbud'); ?></span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($products->get_items($option) as $item) : ?>
            <tr>
                <td><?php ?></td>
                <td>
                    <a href="<?php echo self_admin_url('admin.php?page=shop_products_edit&id=' . $item->id); ?>"><?php echo $item->name; ?></a>
                </td>
                <td><?php echo $item->parent_name; ?></td>
                <td><?php echo $item->display_name; ?></td>
                <td><?php echo __($item->published ? 'Opublikowano' : 'Nie opulikowano', 'cerbud'); ?></td>
                <td><?php echo $item->price; ?></td>
                <td><?php echo $item->last_date; ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <?php
    $page_links = paginate_links(array(
        'base' => add_query_arg('pagenum', '%#%'),
        'format' => '',
        'prev_text' => __('&laquo;', 'aag'),
        'next_text' => __('&raquo;', 'aag'),
        'total' => $num_of_pages,
        'current' => $pagenum
    ));

    if ($page_links) {
        echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
    }
    ?>
</div>