<?php
/**
 * @package Sklep Polska Cegła
 */
/*
Plugin Name: Sklep
Plugin URI:
Description: Sklep dla Polskiej Cegły (Cerbud)
Version: 1.0.0
Author: Dawid Pych
Author URI:
License:
Text Domain: sklep
*/

/*
System umożliwiający wystawianie ofertowe oraz możliwość wysyłania
przez klientów zapytań produktowych.
*/

include_once('library/factory.php');
include_once('library/factory_admin.php');
include_once('library/order.php');
include_once('library/clients.php');
include_once('library/category.php');
include_once('library/product.php');
include_once('library/admin/nav-menus.php');
include_once('helper/widget-menu.php');
include_once('helper/form.php');

define('CERBUD_PLUGIN_DIR', plugin_dir_path(__FILE__));

class Cerbud_Sklep
{

    /**
     * Ustawienia dla roli vendor-ów.
     * @var array
     */
    protected $vendor_attr = array(
        'name' => 'vendor',
        'attr' => array(
            'read' => true,
            'level_0' => true,
            'vendor' => true
        )
    );
    
    /**
     * Nazwa dla ciasteczka do przechowywanie koszyka.
     * @var type 
     */
    protected $cookie_name = 'basket';

    /**
     * Methoda statyczna dla działań w trakcie installacji.
     */
    public static function install()
    {
        Cerbud_Sklep::factory()->create_roles();
    }

    /**
     * Metoda statyczna dla działań w trakcie odistalowania.
     */
    public static function uninstall()
    {
        Cerbud_Sklep::factory()->remove_roles();
    }

    /**
     * Methoda inistalizacji działań dla Wordpressa.
     */
    public static function init()
    {
        register_activation_hook(__FILE__, array('Cerbud_Sklep', 'install'));
        register_deactivation_hook(__FILE__, array('Cerbud_Sklep', 'uninstall'));
        add_action('generate_rewrite_rules', array('Cerbud_Sklep', 'create_rewrite'));
        add_action('init', array('Cerbud_Sklep', 'flush_rewrite'));
        add_action('query_vars', array('Cerbud_Sklep', 'create_variables'));
        add_action('template_include', array('Cerbud_Sklep', 'page_template'));
        add_action('widgets_init', array('Cerbud_Sklep', 'add_widgets'));
        add_action('admin_init', array('Cerbud_Sklep_Nav_Menu', 'add_nav_menu_meta_boxes'));
        add_action('admin_init', array('Cerbud_Sklep', 'add_caps_to_roles'));
        add_action('admin_print_scripts', 'Cerbud_Sklep_Form::media_scripts');
        add_action('admin_print_styles', 'Cerbud_Sklep_Form::media_style');
    }

    /**
     * Tworzenie rewrite.
     * @return array
     */
    public static function create_rewrite()
    {
        global $wp_rewrite;

        $wp_rewrite->add_rewrite_tag('%product%', '([0-9]*)', 'product=');
        $wp_rewrite->add_rewrite_tag('%category%', '([^&]+)', 'category=');
        $wp_rewrite->add_rewrite_tag('%client%', '([^&]+)', 'client=');
        $wp_rewrite->add_rewrite_tag('%basket%', '(show|add|remove)', 'basket=');

        $new_rules = array(
            "product/?$" => 'index.php?product=$matches[1]',
            "category/?$" => 'index.php?category=$matches[1]',
            "client/?$" => 'index.php?client=$matches[1]',
            "basket" => "index.php?basket=show",
            "basket/add" => "index.php?basket=add",
            "basket/remove" => "index.php?basket=remove",
            "category/?([^&]+)/product/?$" => 'index.php?category=$matches[1]&product=$matches[2]',
            "client/?([^&]+)/category/?([^&]+)$" => 'index.php?client=$matches[1]&category=$matches[2]',
            "client/?([^&]+)/category/?([^&]+)/product/?$" => 'index.php?client=$matches[1]&category=$matches[2]&product=$matches[3]'
        );

        $wp_rewrite->rules = $new_rules + $wp_rewrite->rules;

        return $wp_rewrite->rules;
    }

    /**
     * Dla wprowadzenia nowych opcji w rewrite.
     */
    public static function flush_rewrite()
    {
        global $wp_rewrite;

        $wp_rewrite->flush_rules();
    }

    /**
     * Nowe zmienne dla nowych metod rewrite.
     * @return array
     */
    public static function create_variables($public_query_vars)
    {
        
        $public_query_vars[] = 'product';
        $public_query_vars[] = 'category';
        $public_query_vars[] = 'client';
        $public_query_vars[] = 'basket';

        return $public_query_vars;
    }

    /**
     * Obsługa nowych template.
     * @return string
     */
    public static function page_template($template)
    {
        global $wp_query;
        
//        $basket = array(
//            1 => array(
//                item_id => 1,
//                item_value => 100,
//                item_price => 1.23
//            ),
//            2 => array(
//                item_id => 2,
//                item_value => 100,
//                item_price => 2, 34
//            )
//        );
        
//        setcookie('basket', base64_encode(json_encode($basket)), time() + (7 * 86400));
        if (isset($wp_query->query_vars['basket']) && $wp_query->query_vars['basket'] == 'send') {
            Cerbud_Sklep::factory()->send_order();
        }
        
        if (isset($wp_query->query_vars['product'])) {
            $page_template = self::get_template('product');
        } elseif (isset($wp_query->query_vars['category'])) {
            $page_template = self::get_template('category');
        } elseif (isset($wp_query->query_vars['client'])) {
            $page_template = self::get_template('client');
        } elseif (isset($wp_query->query_vars['basket']) && $wp_query->query_vars['basket'] == 'show') {
            $page_template = self::get_template('basket');
        } elseif (isset($wp_query->query_vars['basket']) && $wp_query->query_vars['basket'] == 'add') {
            Cerbud_Sklep::factory()->add_to_basket();
        } elseif (isset($wp_query->query_vars['basket']) && $wp_query->query_vars['basket'] == 'remove') {
            Cerbud_Sklep::factory()->remove_from_basket();
        } elseif (isset($wp_query->query_vars['basket']) && $wp_query->query_vars['basket'] == 'send') {

        } elseif (isset($wp_query->query_vars['basket']) && $wp_query->query_vars['basket'] == 'json') {
            Cerbud_Sklep::factory()->get_cookies();
        } else {
            $page_template = null;
        }

        exit();
        
        if (!$page_template) {
            //$page_template = dirname( __FILE__ ) . '/templates/custom-page-template.php';
            return $template;
        } elseif ($page_template) {
            return $page_template;
        } else {
            return $template;
        }
    }

    /**
     * Tutaj dodajemy rejestrowanie nowych widget-ów.
     */
    public static function add_widgets()
    {
        register_widget('WP_Widget_Menu_Cerbud_Sklep');
    }

    /**
     * Dodaje vendor dla administratora.
     */
    public function add_caps_to_roles()
    {
        $role = get_role( 'administrator' );
        $role->add_cap('vendor');
    }

    /**
     * Dodanie produktów do koszyka.
     */
    private function add_to_basket()
    {
        global $products;

        $basket = array();
        
        $product_id = $_POST['product_id'];
        $product_value = $_POST['product_value'];

        $cookie = $_COOKIE;

        if (!isset($cookie[$this->cookie_name])) {
            setcookie($this->cookie_name, base64_encode(json_encode($basket)), time() + (7 * 86400));
        } else {
            $basket = json_decode(base64_decode($cookie[$this->cookie_name]), true);
        }

        $product = $products->get_item(
            array(
                'id' => htmlentities($product_id)
            )
        );
        
        $basket[(string) $product_id] = array(
            'item_id' => $product_id,
            'item_value' => $product_value,
            'item_price' => $product->price
        );
        
        setcookie($this->cookie_name, base64_encode(json_encode($basket)), time() + (7 * 86400));
        
        wp_redirect(home_url() . '?basket=' . $_POST['action']);

        exit;

    }

    private function get_cookies() {
        $cookie = $_COOKIE;
        
        echo base64_decode($cookie[$this->cookie_name]);
        exit;
    }
    
    /**
     * Usunięcie elementu z koszyka.
     */
    private function remove_from_basket()
    {
        global $product;
        
        $cookie = $_COOKIE;

        $basket = json_decode(base64_decode($cookie[$this->cookie_name]));
        
        $post = explode($_POST['ids'], ',');

        foreach ($post as $id) {
            unset($basket[(int)$id]);
        }

        setcookie($this->cookie_name, base64_encode(json_encode($basket)), time() + (7 * 86400));
        
        wp_redirect(home_url() . '?basket=show');

        exit;
    }

    private function send_order()
    {
        $order_id = md5(date('Ymshis') . rand(0, 1000));
        $post = $_POST['client'];

        $id = Cerbud_Sklep::factory()->create_client($order_id);

        $html = '';

        if ($id) {
            Cerbud_Sklep::factory()->create_order($order_id, $id);

            wp_mail(
                $post['email'],
                __('Order derail', 'sklepcerbud'),
                $html,
                array('Content-Type: text/html; charset=UTF-8')
            );

            setcookie($this->cookie_name, base64_encode(json_encode(array())), time() + (7 * 86400));
        }
        
        wp_redirect(home_url() . '?basket=show');
    }

    /**
     * Create new client and return id of new client
     *
     * @param $order_id
     * @return mixed
     */
    private function create_client($order_id)
    {
        global $clients;

        $post = $_POST['client'];

        $c = array(
            'order_id' => $order_id,
            'name' => htmlentities($post['name']),
            'adres' => htmlentities($post['adres']),
            'order_adres' => htmlentities($post['order_adres']),
            'phone' => htmlentities($post['phone']),
            'email' => htmlentities($post['email']),
            'info' => htmlentities($post['info']),
            'params' => json_encode(
                array('regulamin' => 'true')
            )
        );
        
        return $clients->add_item($c);
    }

    private function create_order($order_id, $client_id)
    {
        global $orders;
        
        $basket = json_decode(base64_decode($_COOKIE[$this->cookie_name]), true);

        var_dump($basket);
        
        foreach ($basket as $item) {

            $option = array(
                'order_id' => $order_id,
                'product_id' => $item['item_id'],
                'vendor_id' => $item['item_vendor'],
                'client_id' => $client_id,
                'value' => $item['item_value'],
                'price' => $item['item_price'],
                'status' => 1
            );

            $orders->add_item($option);
        }
    }

    /**
     * Zwracja instancje klassy.
     * @return Cerbud_Sklep
     */
    private static function factory()
    {
        return new Cerbud_Sklep();
    }

    /**
     * Tworzenie roli
     */
    private function create_roles()
    {
        add_role(
            $this->vendor_attr['name'],
            __($this->vendor_attr['name'], 'sklepcerbud'),
            $this->vendor_attr['attr']
        );
    }

    /**
     * Usuwanie roli
     */
    private function remove_roles()
    {
        remove_role($this->vendor_attr['name']);
    }

    /**
     * Pobieranie linku dla templatek
     * @param $type
     * @return string
     */
    private function get_template($type)
    {       
        $url1 = get_template_directory_uri() . '/sklep-' . $type . '.php';
        $url2 = dirname(__FILE__) . '/templates/sklep-' . $type . '.php';

        if (is_file($url1)) {
            return $url1;
        } else {
            return $url2;
        }
    }
}

Cerbud_Sklep::init();