<?php
global $products;

$product = $products->get_item(array('id' => (int) $wp_query->query_vars['product']));
$product->params = json_decode($product->params);

$cookie = base64_decode($_COOKIE['basket']);
$cookie = json_decode($cookie, true);
?>
<?php get_header(); ?>
<section id="sklep-produkt" class="container">
    <div class="row">
        <div class="col-xs-12">
            <h2>
                <?php echo $product->name; ?>
                <br />
                <small><?php echo __('Typ:','sklepcerbud'); ?>; <?php echo __('Klasa:','sklepcerbud'); ?></small>
            </h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <?php ?>
            <img class="img-responsive" src="" alt="" />
            <?php ?>
        </div>
        <div class="col-md-4">
            <h3><?php echo __('Cena brutton', 'sklepcerbud'); ?></h3>
            <div class="price">
                <p><?php echo $product->price ?> <?php echo __('zł', 'sklepcerbud'); ?></p>
            </div>
            <div class="status">
                <p><?php echo __('Dostępny', 'sklepcerbud'); ?></p>
            </div>
            <form action="<?php the_permalink(); ?>/?basket=add" method="post">
                <div class="input-group">
                    <input type="text" class="form-control" name="product_value" value="<?php echo $cookie[(int) $wp_query->query_vars['product']] ? $cookie[$wp_query->query_vars['product']]['item_value'] : 1; ?>">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit">Do koszyka</button>
                    </span>
                </div>
                <input type="hidden" class="form-control" name="product_id" value="<?php echo $wp_query->query_vars['product']; ?>">
                <input type="hidden" class="form-control" name="action" value="show">
            </form>
        </div>
        <div class="col-md-6">
            <h3>
                <?php echo __('Charakterystyka produktu', 'sklepcerbud'); ?>
            </h3>
            <dl class="dl-horizontal">
                <dt><?php echo __('Ceny brutto:','sklepcerbud'); ?></dt>
                <dd>1,75 <?php echo __('zł','sklepcerbud'); ?></dd>
                <dt></dt>
                <dd><?php echo __('Dostępny','sklepcerbud'); ?></dd>
            </dl>
        </div>
    </div>
</section>
<?php get_footer(); ?>
