<?php 
$basket = json_decode(base64_decode($_COOKIE['basket']), true);
?>
<?php get_header(); ?>
<section id="sklep-basket" class="container">
    <div class="row">
        <div class="col-xs-12">
            <h3>Koszyk</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-hover table-strip">
                <thead>
                    <tr>
                        <th width="40">
                            <?php echo __('Lp.', 'sklepcerbud'); ?>
                        </th>
                        <th>
                            <?php echo __('Nazwa produktu', 'sklepcerbud'); ?>
                        </th>
                        <th width="160">
                            <?php echo __('Ilość', 'sklepcerbud'); ?>
                        </th>
                        <th width="80">
                            <?php echo __('Cena','sklepcerbud'); ?>
                        </th>
                        <th width="40"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;
                    foreach( $basket as $key=>$product) :
                        $total_price = ( $product['item_value'] * $product['item_price'] ) + $total_price;
                        $i++;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $product['item_id']; ?></td>
                        <td>
                            <div class="form-group">
                                <form action="<?php echo home_url() . '?basket=add' ?>" method="POST">
                                    <div class="input-group">
                                        <input type="text" name="product_value" class="form-control" value="<?php echo $product['item_value'] ?>" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="submit">Zmień</button>
                                        </span>
                                    </div>
                                    <input type="hidden" name="product_id" value="<?php echo $product['item_id'] ?>" />
                                    <input type="hidden" name="action" value="show" />
                                </form>
                            </div>
                        </td>
                        <td><?php echo $product['item_value'] * $product['item_price']; ?></td>
                        <td>
                            <form action="<?php echo home_url() . '?basket=remove' ?>" method="POST">
                                <button type="submit" class="btn btn-default">X</button>
                                <input type="hidden" name="ids" value="<?php echo $product['item_id'] ?>" />
                            </form>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3" class="text-right"><?php echo __('Razem: ','sklepcerbud'); ?></td>
                        <td><?php echo $total_price; ?></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <form action="<?php echo home_url() . '?basket=send' ?>" method="post" >
                <div class="form-group">
                    <label><?php echo __('Imię i nazwisko: ', 'sklepcerbud'); ?></label>
                    <input class="form-control" type="text" name="client[name]" value="">
                </div>
                <div class="form-group">
                    <label><?php echo __('Adres: ', 'sklepcerbud'); ?></label>
                    <input class="form-control" type="text" name="client[adres]" value="">
                </div>
                <div class="form-group">
                    <label><?php echo __('Adres dostawy: ', 'sklepcerbud'); ?></label>
                    <input class="form-control" type="text" name="client[order_adres]" value="">
                </div>
                <div class="form-group">
                    <label><?php echo __('Telefon: ', 'sklepcerbud'); ?></label>
                    <input class="form-control" type="text" name="client[phone]" value="">
                </div>
                <div class="form-group">
                    <label><?php echo __('Email: ', 'sklepcerbud'); ?></label>
                    <input class="form-control" type="text" name="client[email]" value="">
                </div>
                <div class="form-group">
                    <label><?php echo __('Uwagi: ', 'sklepcerbud'); ?></label>
                    <textarea class="form-control" name="client[info]"></textarea>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="client[params][regulamin]"> <?php echo __('Potwierdzam, że zapoznałem się z regulaminem zakupów w portalu Polska Cegła:','sklepcerbud'); ?>
                    </label>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary"><?php echo __('Wyślij zapytanie','sklepcerbud'); ?></button>
                </div>
            </form>
        </div>
    </div>
</section>
<?php get_footer(); ?>
