<?php get_header(); ?>
<section id="sklep-category" class="container">
    <div class="row">
        <div class="col-md-4">

        </div>
        <div class="col-md-8">
            <form class="form-horizontal" method="post" action="">
                <div class="form-group">
                    <label class="col-sm-3"><?php echo __('Zastosowanie:','sklepcerbud')?></label>
                    <div class="col-sm-9">
                        <select class="form-control">

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3"><?php echo __('Rodzaj:','sklepcerbud'); ?></label>
                    <div class="col-sm-9">
                        <select class="form-control">

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <label>
                            <input type="checkbox"> <?php echo __('Promocja:','sklepcerbud'); ?>
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button class="btn btn-primary"><?php echo __('Szukaj','sklepcerbud'); ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h3>
                <?php echo __('Produkty','sklepcerbud'); ?>
            </h3>
        </div>
    </div>
    <?php foreach( $categories->get_products() as $product ) : ?>
    <div class="row">
        <div class="col-xs-12">
            <h3>
                Product name
            </h3>
        </div>
        <div class="col-md-3">
            <?php ?>
            <img class="img-responsive" src="" alt="" />
            <?php ?>
        </div>
        <div class="col-md-6">
            <dl class="dl-horizontal">
                <?php ?>
                <dt><?php echo __('Atr','sklepcerbud'); ?></dt>
                <dd>1,75</dd>
                <?php ?>
            </dl>
        </div>
        <div class="col-md-3">
            <dl class="dl-horizontal">
                <dt><?php echo __('Ceny brutto:','sklepcerbud'); ?></dt>
                <dd>1,75 <?php echo __('zł','sklepcerbud'); ?></dd>
                <dt></dt>
                <dd><?php echo __('Dostępny','sklepcerbud'); ?></dd>
            </dl>
        </div>
        <div class="col-md-4 col-md-offset-8">
            <a href="" class="btn btn-default"><?php echo __('Szczegóły','sklepcerbud'); ?></a>
            <a href="#" class="btn btn-default"><?php echo __('Do koszyka','sklepcerbud'); ?></a>
        </div>
    </div>
    <?php endforeach; ?>
</section>
<?php get_footer(); ?>
